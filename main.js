const {getCoinMarketCap,multiple} = require('./exchanges/retrieveexchange');
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

//var index = require('./routes/index');
  //var users = require('./routes/users');

var app = express();

var router = express.Router();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
// app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));

// app.use('/', index);
// app.use('/users', users);

// // catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   var err = new Error('Not Found');
//   err.status = 404;
//   next(err);
// });

// // error handler
// app.use(function(err, req, res, next) {
//   // set locals, only providing error in development
//   res.locals.message = err.message;
//   res.locals.error = req.app.get('env') === 'development' ? err : {};

//   // render the error page
//   res.status(err.status || 500);
//   res.render('error');
// });

// app.listen(3001, function (req, res) {
//   // console.log('Example app listening on new port 3001!')
//   // console.log('add is', add(1,2));
//   // console.log('multiple is', multiple(1,4));
//   res.json({ message: 'hooray! welcome to our api!' }); 
// })

var port = process.env.PORT || 8081;

router.get('/', function(req, res) {
    res.send('Welcome to Matrix Server. Yay!')   
});

router.get('/coinmarketcap', function(req, res) {
    //console.log('coins in app is ', add(3,5));
      getCoinMarketCap('coinmarketcap').then((d) =>{
        res.json(d)
      }); 
});

router.get('/:exchangename', function(req, res) {
    let exchange = req.url.replace(/\//g, '');
    console.log('exchange name now now is ', exchange)
      getMarket(exchange).then((d) =>{
        res.json(d)
      }); 
});

app.use('/api', router);

app.listen(port);
console.log('Magic happens on port ' + port);

module.exports = app;

"use strict";

var axios = require('axios');
var Firebase = require("firebase");
var Firebasedmin = require("firebase-admin");
var serviceAccount = require("./keys/matrixapp-f42cb-firebase-adminsdk-pc3nr-27a82231c4.json");

Firebasedmin.initializeApp({
  credential: Firebasedmin.credential.cert(serviceAccount),
  databaseURL: "https://matrixapp-f42cb.firebaseio.com"
});

var matrixFirebase = Firebasedmin.database();

//console.log('Matrix fire base is ', matrixFirebase)

var exchangesRef = matrixFirebase.ref('exchanges');

//console.log(exchangesRef)

exchangesRef.child('poloniex').update({
  //coins: [{name:"Bitcoin", value:"4000.00"},{name:"Ethereum", value:"350.00"},{name:"Ripple", value:"150.00"}]
  bitcoin: { symbol: "BTC", value: "4005.00" },
  ethereum: { symbol: "ETH", value: "355.00" },
  ripple: { symbol: "RIP", value: "155.00" }
});

exchangesRef.child("poloniex/bitcoin/value").on("value", function (snapshot) {
  console.log(snapshot.val());
});
console.log('Done');
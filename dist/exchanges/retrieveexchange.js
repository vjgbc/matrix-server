'use strict';

var _require = require('../lib/constants'),
    exchangesRef = _require.exchangesRef;

// console.log('exchange is ' , exchangesRef)
// exchangesRef.child("coinmarketcap").on("value", function(snapshot) {
//   console.log(snapshot.val()); 
// });


var keyValue = 'bittrex';
getCoinMarketCap = function getCoinMarketCap() {

  return exchangesRef.child('coinmarketcap').once('value').then(function (snapshot) {
    // The Promise was "fulfilled" (it succeeded).
    var returnArr = [];
    snapshot.forEach(function (childSnapshot) {
      var item = childSnapshot.val();
      // item.key = childSnapshot.key;
      returnArr.push(item);
    });
    // console.log('return array ', returnArr);
    return returnArr;
  }, function (error) {
    // The Promise was rejected.
    console.error(error);
  });
};

getMarket = function getMarket(exchange) {

  return exchangesRef.child(exchange).once('value').then(function (snapshot) {
    // The Promise was "fulfilled" (it succeeded).
    var returnArr = [];
    snapshot.forEach(function (childSnapshot) {
      var item = childSnapshot.val();
      // item.key = childSnapshot.key;
      returnArr.push(item);
    });
    //console.log('return array ', returnArr);
    return returnArr;
  }, function (error) {
    // The Promise was rejected.
    console.error(error);
  });
};

module.exports = {
  getCoinMarketCap: getCoinMarketCap, getMarket: getMarket
};
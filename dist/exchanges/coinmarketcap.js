'use strict';

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var axios = require('axios');

var _require = require('../lib/constants'),
    exchangesRef = _require.exchangesRef;

// console.log('exchange is ' , exchangesRef)
// exchangesRef.child("coinmarketcap/btc/price_usd").on("value", function(snapshot) {
//   console.log(snapshot.val()); 
// });

axios.get('https://api.coinmarketcap.com/v1/ticker/').then(function (response) {
  // console.log(response.data[0]);
  response.data.map(function (coin) {
    console.log(coin.name);
    if (/^[a-zA-Z0-9]*$/.test(coin.symbol) == true) {
      exchangesRef.child('coinmarketcap').update(_defineProperty({}, coin.symbol.toLowerCase(), { symbol: coin.symbol, name: coin.name, id: coin.name.toLowerCase(), price_usd: coin.price_usd, price_btc: coin.price_btc, market_cap_usd: coin.market_cap_usd,
        percent_change_24h: coin.percent_change_24h, available_supply: coin.available_supply, total_supply: coin.total_supply }));
    }
  });
}).catch(function (error) {
  console.log(error);
});
'use strict';

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var axios = require('axios');

var _require = require('../lib/constants'),
    exchangesRef = _require.exchangesRef;

// console.log('exchange is ' , exchangesRef)
// exchangesRef.child("coinmarketcap/btc/price_usd").on("value", function(snapshot) {
//   console.log(snapshot.val()); 
// });

// axios.get('https://api.cryptonator.com/api/full/eth-usd')
//   .then(function (response) {
//         console.log(response);
//         console.log('-------------------------');
//         console.log(response.data.ticker.markets);
//         response.data.ticker.markets.map(each_market => {
//           console.log(each_market.market);
//          if(/^[a-zA-Z0-9]*$/.test(each_market.market) == true)
//         {
//           let percent_ch_24h = response.data.ticker.change / each_market.price * 100;
//           console.log('24_h price is', percent_ch_24h)
//           exchangesRef.child(each_market.market.toLowerCase()).update({
//             eth:{symbol:"eth", name:"Ethereum", price_usd:each_market.price, percent_change_24h:percent_ch_24h}          
//           });
//         }
//         })
//       })
//   .catch(function (error) {
//     console.log(error);
//   });

axios.get('https://api.coinmarketcap.com/v1/ticker/').then(function (coin_response) {
  // console.log(response.data[0]);
  coin_response.data.map(function (coin) {
    //console.log(coin.symbol);

    axios.get('https://api.cryptonator.com/api/full/' + coin.symbol + '-usd').then(function (response) {
      if (response.data.success) {
        console.log(response.data.ticker.markets);

        response.data.ticker.markets.map(function (each_market) {
          console.log(each_market.market);
          if (/^[a-zA-Z0-9]*$/.test(each_market.market) == true) {
            var percent_ch_24h = response.data.ticker.change / each_market.price * 100;
            console.log('24_h price is', percent_ch_24h);
            exchangesRef.child(each_market.market.toLowerCase()).update(_defineProperty({}, coin.symbol.toLowerCase(), { symbol: coin.symbol, name: coin.name, id: coin.name.toLowerCase(), price_usd: each_market.price, percent_change_24h: percent_ch_24h }));
          }
        });
      }
    }).catch(function (error) {
      console.log(error);
    });
  });
}).catch(function (error) {
  console.log(error);
});
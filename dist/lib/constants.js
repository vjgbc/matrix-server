"use strict";

var Firebase = require("firebase");
var serviceAccount = require("../keys/matrixapp-f42cb-firebase-adminsdk-pc3nr-27a82231c4.json");

var Firebasedmin = require("firebase-admin");

Firebasedmin.initializeApp({
  credential: Firebasedmin.credential.cert(serviceAccount),
  databaseURL: "https://matrixapp-f42cb.firebaseio.com"
});

var matrixFirebase = Firebasedmin.database();

//console.log('Matrix fire base is ', matrixFirebase)

var exchangesRef = matrixFirebase.ref('exchanges');

module.exports = {
  exchangesRef: exchangesRef
};
var axios = require('axios')
const {exchangesRef} = require('../lib/constants');

// console.log('exchange is ' , exchangesRef)
// exchangesRef.child("coinmarketcap/btc/price_usd").on("value", function(snapshot) {
//   console.log(snapshot.val()); 
// });

axios.get('https://api.coinmarketcap.com/v1/ticker/')
  .then(function (response) {
        // console.log(response.data[0]);
        response.data.map(coin => {
          console.log(coin.name);
         if(/^[a-zA-Z0-9]*$/.test(coin.symbol) == true)
        {
          exchangesRef.child('coinmarketcap').update({
            [coin.symbol.toLowerCase()]:{symbol:coin.symbol, name:coin.name, id: coin.name.toLowerCase(), price_usd:coin.price_usd, price_btc:coin.price_btc, market_cap_usd: coin.market_cap_usd, 
              percent_change_24h:coin.percent_change_24h, available_supply:coin.available_supply, total_supply: coin.total_supply}          
          });
        }
        })
      })
  .catch(function (error) {
    console.log(error);
  });

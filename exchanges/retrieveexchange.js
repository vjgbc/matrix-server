const {exchangesRef} = require('../lib/constants');

// console.log('exchange is ' , exchangesRef)
// exchangesRef.child("coinmarketcap").on("value", function(snapshot) {
//   console.log(snapshot.val()); 
// });
const keyValue = 'bittrex';
getCoinMarketCap = function() {
 
  return exchangesRef.child('coinmarketcap').once('value').then(function(snapshot) {
  // The Promise was "fulfilled" (it succeeded).
    var returnArr = [];
      snapshot.forEach( function(childSnapshot) {
        let item = childSnapshot.val(); 
        // item.key = childSnapshot.key;
        returnArr.push(item);
    });
     // console.log('return array ', returnArr);
      return returnArr;
}, function(error) {
  // The Promise was rejected.
  console.error(error);
});

}

getMarket = function(exchange) {
 
  return exchangesRef.child(exchange).once('value').then(function(snapshot) {
  // The Promise was "fulfilled" (it succeeded).
    var returnArr = [];
      snapshot.forEach( function(childSnapshot) {
        let item = childSnapshot.val(); 
        // item.key = childSnapshot.key;
        returnArr.push(item);
    });
      //console.log('return array ', returnArr);
      return returnArr;
}, function(error) {
  // The Promise was rejected.
  console.error(error);
});

}


module.exports = {
  getCoinMarketCap,getMarket
}